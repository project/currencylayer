<?php

/**
 * @file
 * Contains class CurrencyLayerCurrencyExchanger.
 */

/**
 * Retrieves currency exchange rates from Currencylayer.com.
 */
class CurrencyLayerCurrencyExchanger implements CurrencyExchangerInterface {

  static function getEndpoint() {
    return 'http://apilayer.net/api/';
  }

  /**
   * Implements CurrencyExchangerInterface::load().
   */
  static function load($currency_code_from, $currency_code_to) {
    // We deliberately run this through loadMultiple() rather than request(),
    // because loadMultiple() caches the rates.
    $rates = self::loadMultiple(array(
          $currency_code_from => array($currency_code_to),
    ));

    return $rates[$currency_code_from][$currency_code_to];
  }

  /**
   * Implements CurrencyExchangerInterface::loadMultiple().
   */
  static function loadMultiple(array $currency_codes) {
    $set_cache = FALSE;
    $cached_rates = self::getCache('currency_layer');
    $rates = array();
//    foreach ($currency_codes as $currency_code_from => $currency_codes_to) {
//      foreach ($currency_codes_to as $currency_code_to) {
//        if (empty($cached_rates[$currency_code_from]) || empty($cached_rates[$currency_code_from][$currency_code_to])) {
//          $cached_rates[$currency_code_from][$currency_code_to] = self::request($currency_code_from, $currency_code_to);
//          $set_cache = TRUE;
//        }
//        $rates[$currency_code_from][$currency_code_to] = $cached_rates[$currency_code_from][$currency_code_to];
//      }
//    }
    if ($set_cache) {
      self::setCache('currency_layer', $cached_rates);
    }

    return $rates;
  }

  /**
   * Implements CurrencyExchangerInterface::operationsLinks().
   */
  static function operationsLinks() {
    return array(array(
        'title' => t('configure'),
        'href' => 'admin/config/regional/currency-exchange/currency-layer',
    ));
  }

  /**
   * Returns the codes of all currencies that Currency layer offers rates for.
   *
   * @return array
   *   ISO 4217 currency codes.
   */
  static function currencyCodes() {
    static $currency_codes;
    if (is_array($currency_codes)) {
      return $currency_codes;
    }

    $set_cache = FALSE;
    $cached_codes = self::getCache('currency_layer_codes');
    if (!$cached_codes) {
      $response = self::request('list');
      $set_cache = TRUE;
      $cached_codes = $response->currencies;
    }
    if ($set_cache) {
      self::setCache('currency_layer_codes', (array) $cached_codes);
    }
    $currency_codes = $cached_codes;
    return $currency_codes;
  }

  /**
   * Requests call to Currency layer API.
   *
   * @param string $type
   * @param string $query
   *
   * @return json|NULL
   *   A json object if success, FALSE not.
   */
  protected static function request($type, $query = NULL) {
    $url = self::getEndpoint();
    $response = drupal_http_request($url . '/' . $type . '?access_key=' . variable_get('currency_layer_api_key', '') . $query);
    if ($response->code == 200) {
      return json_decode($response->data);
    }
    watchdog('CurrencyLayerCurrencyExchanger', 'Can`t reach the API!', WATCHDOG_ERROR);
  }

  /**
   * Gets data from the cache.
   *
   * @return array
   *   The requested cache data.
   */
  static function getCache($name) {
    $cache = cache_get($name);
    if (is_object($cache) && $cache->created + self::cacheLifetime() > REQUEST_TIME) {
      return $cache->data;
    }
    return array();
  }

  /**
   * Saves data to the cache.
   *
   * @param array $rates
   *   The saved cache data.
   */
  static function setCache($name, array $data) {
    return cache_set($name, $data, 'cache', time() + self::cacheLifetime());
  }

  /**
   * Returns the cache lifetime.
   *
   * @return int
   */
  static function cacheLifetime() {
    return (int) variable_get('currency_layer_cache_lifetime', CURRENCY_LAYER_CACHE_LIFETIME);
  }

}
